package com.example.fpapara.simplelogin.model;

public class Task {
    private String name;

    public Task() {
        //empty constructor
    }

    public Task(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
