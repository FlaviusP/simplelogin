package com.example.fpapara.simplelogin.ui.adapter;

import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.fpapara.simplelogin.R;
import com.example.fpapara.simplelogin.model.Colors;
import com.example.fpapara.simplelogin.model.Task;

import java.util.List;

public class TaskRecyclerViewAdapter extends RecyclerView.Adapter<TaskRecyclerViewAdapter.DataObjectHolder> {
    private final List<Task> mDataset;
    private static final Colors COLORS = new Colors();

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        final TextView taskName;

        public DataObjectHolder(View itemView) {
            super(itemView);
            taskName = (TextView) itemView.findViewById(R.id.task_name);
        }
    }

    public TaskRecyclerViewAdapter(List<Task> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_task_item, parent, false);
        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        holder.taskName.setText(mDataset.get(position).getName());

        holder.itemView.findViewById(R.id.dotLayout).setBackgroundResource(R.drawable.circle);
        GradientDrawable gradientDrawable = (GradientDrawable) holder.itemView.findViewById(R.id.dotLayout).getBackground().getCurrent();
        gradientDrawable.setColor(COLORS.getNextColor());

        if (position == 0) {
            holder.itemView.findViewById(R.id.up_line).setBackgroundResource(0);
            holder.itemView.findViewById(R.id.down_line).setBackgroundResource(R.drawable.vertical);
            holder.setIsRecyclable(false);
        } else {
            if (position == (mDataset.size() - 1)) {
                holder.itemView.findViewById(R.id.up_line).setBackgroundResource(R.drawable.vertical);
                holder.itemView.findViewById(R.id.down_line).setBackgroundResource(0);
                holder.setIsRecyclable(false);
            } else {
                holder.itemView.findViewById(R.id.up_line).setBackgroundResource(R.drawable.vertical);
                holder.itemView.findViewById(R.id.down_line).setBackgroundResource(R.drawable.vertical);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}
