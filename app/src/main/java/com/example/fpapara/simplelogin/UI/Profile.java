package com.example.fpapara.simplelogin.ui;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.fpapara.simplelogin.model.Task;
import com.example.fpapara.simplelogin.R;
import com.example.fpapara.simplelogin.ui.adapter.TaskRecyclerViewAdapter;

import java.util.ArrayList;

public class Profile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getBaseContext(), getString(R.string.add_task), Toast.LENGTH_SHORT).show();
                }
            });
        }
        ArrayList<Task> data = fillWithData();
        TaskRecyclerViewAdapter adapter;
        adapter = new TaskRecyclerViewAdapter(data);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.taskRecyclerView);
        final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        if (recyclerView != null) {
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);
        }
    }

    private ArrayList<Task> fillWithData() {

        ArrayList<Task> results = new ArrayList<>();

        Task task = new Task(getString(R.string.task1));
        results.add(task);

        task = new Task(getString(R.string.task2));
        results.add(task);

        task = new Task(getString(R.string.task3));
        results.add(task);

        task = new Task(getString(R.string.task4));
        results.add(task);

        return results;
    }

}
