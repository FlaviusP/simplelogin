package com.example.fpapara.simplelogin.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.fpapara.simplelogin.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText mUserName;
    private EditText mPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button loginBtn;

        mUserName = (EditText) findViewById(R.id.userName);
        mPassword = (EditText) findViewById(R.id.password);
        loginBtn = (Button) findViewById(R.id.loginButton);

        if (loginBtn != null) {
            loginBtn.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.loginButton && (dataIsValid())) {
            startActivity(new Intent(getBaseContext(), Profile.class));
        }
    }

    private boolean dataIsValid() {
        String userName = mUserName.getText().toString().trim();
        String password = mPassword.getText().toString().trim();

        if (userName.isEmpty() && password.isEmpty()) {
            mUserName.setError(getString(R.string.err_invalid_user));
            mUserName.requestFocus();
            return false;
        }
        if (!isValidEmail(userName)) {
            Toast.makeText(this, getString(R.string.err_invalid_user), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            if (!checkUser(userName)) {
                Toast.makeText(this, getString(R.string.err_invalid_user), Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        if (password.length() < 6) {
            Toast.makeText(this, getString(R.string.err_invalid_pass), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            if (!checkPassword(password)) {
                Toast.makeText(this, getString(R.string.err_invalid_pass), Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        return true;
    }

    private boolean isValidEmail(String emailInput) {
        if (TextUtils.isEmpty(emailInput)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(emailInput).matches();
        }
    }

    private boolean checkUser(String userInput) {
        return userInput.equals(getString(R.string.user));
    }

    private boolean checkPassword(String passwordInput) {
        return passwordInput.equals(getString(R.string.password));
    }
}
