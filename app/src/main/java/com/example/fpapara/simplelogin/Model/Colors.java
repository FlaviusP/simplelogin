package com.example.fpapara.simplelogin.model;

public class Colors {

    private static int curentColor=0;
    private static final int NUMBER_OF_COLORS = 9;
    private static boolean isFirst=true;
    private static final int[] COLOR = {
            android.graphics.Color.BLUE,
            android.graphics.Color.CYAN,
            android.graphics.Color.DKGRAY,
            android.graphics.Color.GRAY,
            android.graphics.Color.GREEN,
            android.graphics.Color.LTGRAY,
            android.graphics.Color.MAGENTA,
            android.graphics.Color.RED,
            android.graphics.Color.YELLOW
    };

    public Colors() {
        curentColor = 0;
        isFirst = true;
    }

    public static int getNextColor() {
        if (isFirst) {
            isFirst = false;
            return COLOR[curentColor];
        }
        if (curentColor < (NUMBER_OF_COLORS - 2)) {
            curentColor++;
            return COLOR[curentColor];
        } else {
            curentColor = 0;
            return COLOR[curentColor];
        }
    }
}
